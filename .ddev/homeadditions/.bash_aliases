alias l="ls -lha"
alias lt="ls -ltha"

# Shortcuts paths
alias ..="cd .."
alias home="cd /var/www/html"
alias goweb="cd /var/www/html/web"
alias gomodules="cd /var/www/html/web/modules/custom"
alias goprofiles="cd /var/www/html/web/themes/custom"
alias gosites="cd /var/www/html/web/sites/default"
alias gothemes="cd /var/www/html/web/themes/custom"

# Git related
alias gitreset0="git reset --hard && git clean -ffdx"
